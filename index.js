/*jshint esversion: 6 */

const anime = require('animejs');
const scrollWatcher = require('scroll-watcher')
const debut = false
const debugView = null

const scroll = new scrollWatcher();

var uiListItems = [].slice.call(document.querySelectorAll('.uiListItem'));
var buttons = [].slice.call(document.querySelectorAll('.button'));
var uiSegmentGroups = [].slice.call(document.querySelectorAll('.jsSegmentControlGroup'))



uiSegmentGroups.forEach((el, i) => {
  children = [].slice.call(el.children);

  segmentControls = []
  segmentViews = []

  children.forEach((child, i) => {
    if (child.classList.contains('uiSegmentControls')) {
      segmentControls = [].slice.call(child.children)
    }
    if (child.classList.contains('uiSegmentControlViews')){
      segmentViews = [].slice.call(child.children)
    }
  })

  function setActiveIndex(index) {
    segments = [[...segmentControls], [...segmentViews]]
    segments.forEach((group, i) => {
      group.forEach((item, i) => {
        if (i === index) {
          item.classList.add("active")
          item.classList.remove("inactive")
        } else {
          item.classList.add("inactive")
          item.classList.remove("active")
        }
      })
    })
  }

  segmentControls.forEach((segment, i) => {
    segment.addEventListener('click', function (e) {
      e.preventDefault()
      setActiveIndex(i)
      anime({
        targets: segmentViews[i],
        opacity: [0, 1],
        maxHeight: [0, 'none'],
        translateY: [16,0],
        complete: () => console.log('did')
      })
    });
  })

  setActiveIndex(0)
})


uiListItems.forEach((el, i) => {
  scroll.watch(el)
  .once('enter', function(evt){
    anime({
      targets: el,
      translateX: [-32, 0],
      opacity: [0, 1],
    })
  })
})

if (debug) {
  debugView = document.createElement('div');
}


var data = null;

var state = {
  sliceAreaIndex : 0,
  sliceAreaMax : 12
}

function getData() {
  var data = fetch('https://jsonplaceholder.typicode.com/posts')
  return data.then(function(output){
    return output.json()
  })

}

getData().then((res) => {
  data = res
})

function renderContent(data, parent) {
    parent.innerHTML = null;
    data.forEach(function(item, index){
      el = document.createElement("div");
      el.classList = "content animContent";
      el.textContent = item.body;
      parent.appendChild(el);

      anime({
        targets: parent.children,
        keyframes: [
          {translateY: 32, opacity: 0, duration: 0},
          {translateY: 0, opacity: 1}
        ],
        delay: anime.stagger(80)
      })
    })
  return parent.children
}

function paginate(data) {
  data = data.slice(state.sliceAreaIndex, state.sliceAreaIndex + state.sliceAreaMax);
  if (debug) {
    debugView.textContent = state.sliceAreaIndex.toString();
  }
  if (data.length !== 0) {
    state.sliceAreaIndex = state.sliceAreaIndex + state.sliceAreaMax;
  } else {
    state.sliceAreaIndex = 0;
  }
  return data
}

function renderButton(label, parent) {
  btn = document.createElement('button')
  btn.classList = "button"
  btn.textContent = label
  parent.appendChild(btn)
  return btn
}


// getData().then((res) => {
//   data = res
//   grid = document.createElement('div')
//   console.log(grid)
//   grid.id = "grid"
//   grid.classList = "grid"
//   document.getElementById('app').appendChild(grid)
//   console.log(debug)
//   renderContent(paginate(data), grid)
//   btn = renderButton('See More', document.getElementById('app'))
//   btn.addEventListener('click', () => {
//       content = renderContent(paginate(data), grid)
//     })
// })

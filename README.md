# Nest ⌁ UI

## Setup
1. `npm install`
2. Uses Budo as a prototyping server
3. `npm install -g budo` to install globally


## Run

`npm start`


## Colophon

Includes [Space Grotesk](https://fonts.floriankarsten.com/space-grotesk) by Florian Karsten, licensed under the SIL Open Font License.
